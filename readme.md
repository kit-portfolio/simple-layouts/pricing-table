# Pricing table

### Tech stack
* HTML/CSS
* Normalise CSS
* Table layout
* Font awesome

### Demo
Live demo is hosted [here](https://kit-portfolio.gitlab.io/simple-layouts/pricing-table/).

### Launch
To run the project follow the next steps:
* No magic here - simple HTML project

# Preview
![Vertical table preview](preview-vertical.png#center)
![Horizontal table preview](preview-horizontal.png#center)